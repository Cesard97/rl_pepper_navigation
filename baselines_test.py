# --------------------------------------------------------------
# ------------------------- Libraries --------------------------
# --------------------------------------------------------------
import time
import os
import warnings
import math as m
import random
import cv2
import pybullet as pb
import pybullet_data
import numpy as np
import gym
import matplotlib.pyplot as plt

from gym import spaces
from qibullet import PepperVirtual
from qibullet import SimulationManager

from pepper_env import PepperSimEnv

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    from stable_baselines.common.env_checker import check_env
    from stable_baselines.deepq.policies import CnnPolicy
    from stable_baselines import DQN, PPO2, A2C
    from stable_baselines.common.callbacks import BaseCallback
    from stable_baselines.bench import Monitor
    from stable_baselines.results_plotter import load_results, ts2xy

# --------------------------------------------------------------
# ------------------------- Constants --------------------------
# --------------------------------------------------------------
# Physical Boundaries
MAX_X = 5.7
MIN_X = -3.7
MAX_Y = 5.7
MIN_Y = -5.7


def main_test():
    # Create log dir
    log_dir = "/home/cesar/baselines_results/"
    # Pepper Training Env
    env = PepperSimEnv()
    # Load model
    model = DQN.load(log_dir + "NEW_DDQN_LR0001/best_model.zip")
    # Enjoy trained agent
    obs = env.reset()
    while True:
        action, _states = model.predict(obs)
        obs, rewards, done, info = env.step(action)

        if done:
            obs = env.reset()

if __name__ == "__main__":
    # Main function
    main_test()
