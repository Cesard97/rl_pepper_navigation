#! /usr/bin/env Python3

# --------------------------------------------------------------
# ------------------------- Libraries --------------------------
# --------------------------------------------------------------
import warnings
import time
import cv2
import random
import threading
import pickle
import pybullet as pb
import pybullet_data
import numpy as np
import math as m
import matplotlib.pyplot as plt
from qibullet import PepperVirtual
from qibullet import SimulationManager
from dqn_model import DQNModel

# --------------------------------------------------------------
# ------------------------- Constants --------------------------
# --------------------------------------------------------------
# Physical Boundaries
MAX_X = 5.6
MIN_X = -3.6
MAX_Y = 5.6
MIN_Y = -5.6


class PepperTrainner():
    # --------------------------------------------------------------
    # ----------------------- Initialization -----------------------
    # --------------------------------------------------------------
    def __init__(self, learning_rate=0.01, iterations=100, episodes=1000,
                 gamma=0.95, min_memory=1000, buffer_size=5000, batch_size=50,
                 training_episodes=300, save_info=False, ID=0,
                 load_training=False):
        # Parameters
        self.gamma = gamma
        self.maxIt = iterations
        self.maxEpisode = episodes
        self.min_memory = min_memory
        self.buffer_size = buffer_size
        self.batchSize = batch_size
        self.epsilon = 1
        self.trainingEpisodes = training_episodes
        self.ID = ID
        self.saveResults = save_info
        self.obstacle_prob = 0
        # Initial Pos
        self.pos = [0, 0, 0]
        self.obstacleX = 100
        self.obstacleY = 100
        # Human Positions
        self.humanX = np.ones(3)
        self.humanY = np.ones(3)
        self.lost = True
        self.lostCount = 0
        # Load or initialize model, metrics and results
        if not load_training:
            # Double Q-Learning Model
            self.DQNModel = DQNModel(lr=learning_rate)
            self.TargetDQNModel = DQNModel(lr=learning_rate)
            # Cliped Double Q Models
            # self.DQNModel2 = DQNModel(lr=learning_rate)
            # self.TargetDQNModel2 = DQNModel(lr=learning_rate)
            # Step Memory
            self.faceMemory = []
            self.depthMemory = []
            self.laserMemory = []
            self.actionMemory = []
            self.rewardMemory = []
            self.terminalState = []
            self.lostStates = []
            self.episode = 0
            self.trainingStep = 0
            # Metrics
            self.acumulatedReward = []
            self.endCause = [0, 0, 0, 0, 0]
            self.averageLoss = []
            self.averageAccuracy = []
            self.qValuesAverage = []
            print('New model, memory and metrics initialized')
        else:
            print('Loading model, memory and metrics...')
            loadingID = ID
            # Double Q-Learning Models
            # Main Network
            self.DQNModel = DQNModel(lr=learning_rate)
            self.DQNModel.load_w('DQNModel_'+loadingID)
            self.DQNModel.compile(lr=learning_rate)
            # Target Network
            self.TargetDQNModel = DQNModel(lr=learning_rate)
            self.TargetDQNModel.load_w('DQNTargetModel_'+loadingID)
            self.TargetDQNModel.compile(lr=learning_rate)

            # Double Q-Learning Models
            # Main Network
            # self.DQNModel2 = DQNModel(lr=learning_rate)
            # self.DQNModel2.load_w('DQNModel2_'+loadingID)
            # self.DQNModel2.compile(lr=learning_rate)
            # Target Network
            # self.TargetDQNModel2 = DQNModel(lr=learning_rate)
            # self.TargetDQNModel2.load_w('DQNTargetModel2_'+loadingID)
            # self.TargetDQNModel2.compile(lr=learning_rate)
            # Step Memory from state
            with open('memory/'+str(self.ID)+'/depth.pickle', "rb") as f:
                self.depthMemory = pickle.load(f)
            self.faceMemory = np.genfromtxt('memory/'+loadingID+'/face.csv', delimiter=',').tolist()
            self.laserMemory = np.genfromtxt('memory/'+loadingID+'/laser.csv', delimiter=',').tolist()
            for i, face in enumerate(self.faceMemory):
                self.faceMemory[i] = np.array(face)
                self.laserMemory[i] = np.array(self.laserMemory[i])
            # Step Memory from episode
            self.actionMemory = np.genfromtxt('memory/'+loadingID+'/action.csv', delimiter=',').tolist()
            for i, a in enumerate(self.actionMemory):
                self.actionMemory[i] = int(a)
            self.rewardMemory = np.genfromtxt('memory/'+loadingID+'/reward.csv', delimiter=',').tolist()
            self.terminalState = np.genfromtxt('memory/'+loadingID+'/terminal.csv', delimiter=',').tolist()
            self.lostStates = np.genfromtxt('memory/'+loadingID+'/lost.csv', delimiter=',').tolist()
            # Metrics
            self.acumulatedReward = np.genfromtxt('results/Final_Training/'+loadingID+'_acumulated_reward_DQN.csv',delimiter=',').tolist()
            self.endCause = np.genfromtxt('results/Final_Training/'+loadingID+'_termination_cause_DQN.csv',delimiter=',').tolist()
            self.averageLoss = np.genfromtxt('results/Final_Training/'+loadingID+'_average_loss_DQN.csv',delimiter=',').tolist()
            self.averageAccuracy = np.genfromtxt('results/Final_Training/'+loadingID+'_average_accuracy_DQN.csv',delimiter=',').tolist()
            self.qValuesAverage = np.genfromtxt('results/Final_Training/'+loadingID+'_qValues_average_DQN.csv',delimiter=',').tolist()
            self.episode = len(self.acumulatedReward)
            self.trainingStep = self.episode*50
            print('Model, memory and metrics were loaded succesfully!')

        self.pLostState = (1/(min_memory))*np.ones(min_memory)
        # Simulation
        self.simulation_manager = SimulationManager()
        #self.lock = lock

    # Configuration of the simulation environment
    def set_environment(self):
        print('Setting a new simulation environment...')
        # Client number
        self.client = self.simulation_manager.launchSimulation(gui=True)
        # Create and spawn Pepper in a random position
        init_pos = random.randint(0, 2)
        dX = random.random() - 0.5
        dY = random.random() - 0.5
        if init_pos == 0:
            self.pepper = self.simulation_manager.spawnPepper(
                          self.client, spawn_ground_plane=True,
                          translation=[dX, dY, 0])

        if init_pos == 1:
            self.pepper = self.simulation_manager.spawnPepper(
                          self.client, spawn_ground_plane=True,
                          translation=[3 + dX, 4 + dY, 0],
                          quaternion=[0.0, 0.0, -m.pi/2, m.pi/2])

        if init_pos == 2:
            self.pepper = self.simulation_manager.spawnPepper(
                          self.client, spawn_ground_plane=True,
                          translation=[3 + dX, -4 + dY, 0],
                          quaternion=[0.0, 0.0, -m.pi/2, -m.pi/2])

        self.simulation_manager.setLightPosition(self.client, [0, 0, 1.0])
        # Subscribe to lasers
        self.pepper.showLaser(True)
        self.pepper.subscribeLaser()
        # Creating enviroment
        pb.setAdditionalSearchPath(pybullet_data.getDataPath())
        self.simulation_manager.setLightPosition(self.client, [0, 0, 1.0])
        # Spawn walls
        print('Spawning world')
        pb.loadURDF('wallX.urdf', basePosition=[1, 6.1, 0])
        pb.loadURDF('wallX.urdf', basePosition=[1, -6.1, 0])

        pb.loadURDF('wallY.urdf', basePosition=[-4.1, 0, 0])
        pb.loadURDF('wallY.urdf', basePosition=[6.1, 0, 0])

        # Spawn 3 human randomly
        print('Spawning humans')
        self.humanX[0] = random.random()*4 + 1.7
        self.humanY[0] = random.random()*4 - 2
        pb.loadURDF('humanoid/humanoid.urdf',
                    basePosition=[self.humanX[0], self.humanY[0], 0.85],
                    baseOrientation=[0.0, m.pi/2, m.pi/2, 0.0],
                    globalScaling=0.225, useFixedBase=1)
        self.humanX[1] = random.random()*4 - 3.7
        self.humanY[1] = random.random()*4 - 5.7
        pb.loadURDF('humanoid/humanoid.urdf',
                    basePosition=[self.humanX[1], self.humanY[1], 0.85],
                    baseOrientation=[m.pi/2, m.pi/2, m.pi/2, m.pi/2],
                    globalScaling=0.225, useFixedBase=1)
        self.humanX[2] = random.random()*4 - 3.7
        self.humanY[2] = random.random()*4 + 1.7
        pb.loadURDF('humanoid/humanoid.urdf',
                    basePosition=[self.humanX[2], self.humanY[2], 0.85],
                    baseOrientation=[-m.pi/2, m.pi/2, m.pi/2, -m.pi/2],
                    globalScaling=0.225, useFixedBase=1)
        # Spawn obstacle randomly
        print('Spawning obstacles with probability ' + str(self.obstacle_prob))
        dToObjective = m.sqrt(self.humanX[0]**2 + self.humanY[0]**2)
        if (dToObjective > 3.5) and (random.random() < self.obstacle_prob):
            self.obstacleX = self.humanX[0]/2 + random.random() - 0.5
            self.obstacleY = self.humanY[0]/2 + random.random() - 0.5
            obstacleTheta = random.random()*2*m.pi
            pb.loadURDF(
                'chair_1/chair.urdf',
                basePosition=[self.obstacleX, self.obstacleY, 0.0],
                baseOrientation=[0.0, 0.0, m.pi/2, obstacleTheta],
                globalScaling=1.0, useFixedBase=1)
        time.sleep(0.1)

    # Main
    def main(self):
        print('SARTING SIM ' + str(self.ID) + ' !!! ')
        # --------------------------------------------------------------
        # ----------------------- Training Loop ------------------------
        # --------------------------------------------------------------
        while self.episode < self.maxEpisode:
            # Restart Enviroment
            self.set_environment()
            # ----------------------- Episode Init ------------------------
            print('--------------------------------------------')
            print('         SIM '+str(self.ID)+': EPISODE ' +
                  str(self.episode) + '        ')
            print('--------------------------------------------')
            # Restart variables
            step = 0
            endEpisode = False
            newEpisode = True
            # Restart episode metrics
            episodeLoss = []
            episodeAccuracy = []
            episodeQValues = []
            totalScore = 0
            # Update variable epsilon
            if self.episode and (self.episode < self.trainingEpisodes):
                # linear decay
                # self.epsilon -= 1/self.trainingEpisodes
                # Asymptotical decay
                # self.epsilon = 1/self.episode + 0.01
                # exponential decay
                self.epsilon = 0.05 + 0.95*m.exp(-self.episode/(0.2*self.trainingEpisodes))
            # Greedy policy after training episodes are completed
            if self.episode > self.trainingEpisodes:
                self.epsilon = 0.05

            print('Epsilon (e): ' + str(self.epsilon))
            # --------------------------------------------------------------
            # ----------------------- Episode Loop -------------------------
            # --------------------------------------------------------------
            while not endEpisode:
                print('--------------------------------------------')
                print('SIM '+str(self.ID)+' - Episode ' + str(self.episode))
                print('SIM '+str(self.ID)+' - Episode step: ' + str(step))
                print('SIM '+str(self.ID)+' - Training Step: ' + str(self.trainingStep))
                print('SIM '+str(self.ID)+' - Lost states in memory: ' + str(np.sum(self.lostStates)))
                # Update initial state
                if newEpisode:
                    newState = self.update_state()
                    newEpisode = False

                # Epsilon-greedy movement
                action = self.action_selection(newState, self.epsilon)
                self.move(action)

                # Update state
                prevState = newState
                newState = self.update_state()

                # Get reward and endEpisode condition
                reward, endEpisode = self.get_reward(prevState, newState, step)

                # Save total episode reward for the episode
                totalScore += reward

                # Save step in memory
                self.save_step_in_memory(prevState, action, reward, endEpisode)

                # Verify if memory size is enough to train
                if len(self.faceMemory) >= self.min_memory:
                    # Train DQNModel
                    metrics = self.update_QModel()
                    # Softly update the target network weights
                    self.TargetDQNModel.soft_update(self.DQNModel)
                    # self.TargetDQNModel2.soft_update(self.DQNModel2)
                    # Save episode metrics
                    episodeLoss.append(metrics[0])
                    episodeAccuracy.append(metrics[1])
                    episodeQValues.append(metrics[2])
                # Update step
                step += 1
                self.trainingStep += 1

            # Results from episode
            self.episode += 1
            self.lostCount = 0
            self.averageLoss.append(np.average(np.array(episodeLoss)))
            self.averageAccuracy.append(np.average(np.array(episodeAccuracy)))
            self.qValuesAverage.append(np.average(np.array(episodeQValues)))
            # Score
            self.acumulatedReward.append(totalScore)
            # Saving checkpoint from episode
            if self.saveResults:
                self.save_results()

            # Reset episode
            self.simulation_manager.resetSimulation(self.client)
            self.simulation_manager.stopSimulation(self.client)

    # ---------------------------------------------------------
    # ----------------------- Functions -----------------------
    # ---------------------------------------------------------

    # Save results method
    def save_results(self):
        # Save results
        print('Saving results from episode...')
        np.savetxt('results/Final_Training/'+str(self.ID)+'_acumulated_reward_DQN.csv', self.acumulatedReward, delimiter=',')
        np.savetxt('results/Final_Training/'+str(self.ID)+'_termination_cause_DQN.csv', self.endCause, delimiter=',')
        np.savetxt('results/Final_Training/'+str(self.ID)+'_average_loss_DQN.csv', self.averageLoss, delimiter=',')
        np.savetxt('results/Final_Training/'+str(self.ID)+'_average_accuracy_DQN.csv', self.averageAccuracy, delimiter=',')
        np.savetxt('results/Final_Training/'+str(self.ID)+'_qValues_average_DQN.csv', self.qValuesAverage, delimiter=',')
        # Save replay memory
        print('Saving replay buffer memory...')
        np.savetxt('memory/'+str(self.ID)+'/face.csv', self.faceMemory, delimiter=',')
        with open('memory/'+str(self.ID)+'/depth.pickle', "wb") as f:
            pickle.dump(self.depthMemory, f, pickle.HIGHEST_PROTOCOL)
        np.savetxt('memory/'+str(self.ID)+'/laser.csv', self.laserMemory, delimiter=',')
        np.savetxt('memory/'+str(self.ID)+'/action.csv', self.actionMemory, delimiter=',')
        np.savetxt('memory/'+str(self.ID)+'/reward.csv', self.rewardMemory, delimiter=',')
        np.savetxt('memory/'+str(self.ID)+'/terminal.csv', self.terminalState, delimiter=',')
        np.savetxt('memory/'+str(self.ID)+'/lost.csv', self.lostStates, delimiter=',')
        # Save model
        if len(self.faceMemory) >= self.min_memory:
            print('Saving model...')
            self.DQNModel.save_model('DQNModel_' + str(self.ID))
            self.TargetDQNModel.save_model('DQNTargetModel_' + str(self.ID))
            # self.DQNModel2.save_model('DQNModel2_' + str(self.ID))
            # self.TargetDQNModel2.save_model('DQNTargetModel2_' + str(self.ID))
        print('Episode info has been saved succesfully!')

    #  ----------------------- ACTION  ------------------------
    # Transforming action to movementresultsNewGammaresultsNewGamma
    def movement_action(self, action=0, vel=0.6):
        if action == 0:
            self.pepper.move(vel, 0.0, 0.0)
        elif action == 1:
            self.pepper.move(0.0, vel, 0.0)
        elif action == 2:
            self.pepper.move(0.0, -vel, 0.0)
        elif action == 3:
            self.pepper.move(0.0, 0.0, m.pi/2)
        elif action == 4:
            self.pepper.move(0.0, 0.0, -m.pi/2)

    # Low level control
    def move(self, action):
        # Get previous position
        prevX, prevY, prevW = self.pepper.getPosition()
        # Move
        print('(Action) Pepper is moving: ' + str(action))
        self.movement_action(action)
        time.sleep(0.5)
        # Update position
        self.pos = self.pepper.getPosition()
        start_moving = time.time()
        # Low level control to verify movement
        while ((abs(prevX-self.pos[0]) < 0.3) and
                (abs(prevY-self.pos[1]) < 0.3) and
                (abs(prevW-self.pos[2]) < m.pi/4)) and ((time.time() - start_moving) < 1.5):
            # Retry pepper movement
            # print('Spaming Pepper movement')
            self.movement_action(action)
            self.pos = self.pepper.getPosition()
            time.sleep(0.001)

        # Stop Pepper movement
        self.movement_action(0, 0)
        prevX, prevY, prevW = self.pepper.getPosition()
        time.sleep(0.1)
        # Update position
        self.pos = self.pepper.getPosition()
        start_stoping = time.time()
        # Low level control to verify stop
        while ((abs(prevX-self.pos[0]) > 0.001) or
               (abs(prevY-self.pos[1]) > 0.001) or
               (abs(prevW-self.pos[2]) > 0.001)) and ((time.time() - start_stoping) < 0.5):

            # print('Spaming Pepper STOP')
            self.movement_action(0, 0)
            prevX, prevY, prevW = self.pepper.getPosition()
            time.sleep(0.1)
            self.pos = self.pepper.getPosition()
        # Print position
        print('Pepper stopped moving: ' + str(round(self.pos[0], 1)) + ', ' + str(round(self.pos[1], 1)) + ', ' + str(round(self.pos[2], 1)))

    # Greedy action selection
    def action_selection(self, state, epsilon=0.1):
        action = 0
        qValues = np.zeros(5)
        if random.random() < epsilon:
            action = np.random.randint(5)
        else:
            qValues = self.DQNModel.get_values([state[0].reshape(-1, 2), state[1].reshape(-1, 60, 80,1), state[2].reshape(-1, 45)])
            print(qValues)
            action = np.argmax(qValues)

        return action

    #  ------------------------ STATE  ------------------------
    def update_state(self):
        state = []
        # Get image and mask for the red sphere
        mask = self.get_mask()
        # Get 3D image from the 3D camera
        depth = self.get_depth()
        # Get lasers
        lasers = self.get_laser()
        # Determine of the number of pixels in the mask and normalize
        size = (np.count_nonzero(mask))/1700
        # Minimun threshold
        if size > 0.05:
            # Determine position of the mask and normalize between -1 and 1
            posX = np.average(np.nonzero(mask)[1])/160 - 1
            # Return state
            state.append(np.array([size, posX]))
            state.append(depth)
            state.append(lasers)
            print('SIM '+str(self.ID)+' - (State) Area: ' + str(round(size, 3)) + ', PosX: ' + str(round(posX, 3)))
            return state
        else:
            # Return if lost
            state.append(np.array([0, -1]))
            state.append(depth)
            state.append(lasers)
            print('SIM '+str(self.ID)+' - (State) LOST')
            return state

    # Get mask
    def get_mask(self):
        # Subscribing to the bottom RGB camera
        self.pepper.subscribeCamera(PepperVirtual.ID_CAMERA_TOP)
        # Retrieving and displaying the synthetic image using OpenCV
        img = self.pepper.getCameraFrame()
        # cv2.imshow("Synthetic top camera", img)
        # cv2.waitKey(500)
        # Adding a mask to image to get red pixels
        lower = np.array([0, 0, 100])
        upper = np.array([150, 150, 255])
        mask = cv2.inRange(img, lower, upper)
        # cv2.imshow("Synthetic top camera mask", mask)
        # cv2.waitKey(500)
        self.pepper.unsubscribeCamera(PepperVirtual.ID_CAMERA_TOP)
        return mask/255

    # Get Depth Camera frame
    def get_depth(self):
        # Subscribing to the bottom RGB camera
        self.pepper.subscribeCamera(PepperVirtual.ID_CAMERA_DEPTH)
        # Retrieving and displaying the synthetic image using OpenCV
        img = self.pepper.getCameraFrame()
        # Resize image
        img = cv2.blur(img, (5, 5))
        # cv2.imshow("Syntehtic depth camera", img)
        # cv2.waitKey(500)
        img = cv2.resize(img, (80, 60), interpolation=cv2.INTER_AREA)
        self.pepper.unsubscribeCamera(PepperVirtual.ID_CAMERA_DEPTH)
        return img/8000

    # Get laser information
    def get_laser(self):
        mergedLaser = []
        mergedLaser = np.array(self.pepper.getRightLaserValue(), dtype=np.float32)
        mergedLaser = np.append(mergedLaser, np.array(self.pepper.getFrontLaserValue(), dtype=np.float32))
        mergedLaser = np.append(mergedLaser, np.array(self.pepper.getLeftLaserValue(), dtype=np.float32))

        return mergedLaser/3

    #  ------------------------ REWARD  ------------------------
    def get_reward(self, prevState, newState, step):
        # Variables
        reward = -1

        goalReached = False
        outOfBounds = False
        lostForTooLong = False
        collision = False
        maxItReached = False

        # Change in horizontal position
        deltaPosX = abs(prevState[0][1]) - abs(newState[0][1])

        # Counter if lost
        if all(newState[0] == prevState[0]):
            if self.lost:
                self.lostCount += 1
        # Reward for getting lost
        elif prevState[0][0] != 0 and newState[0][0] == 0:
            self.lost = True
        # Reward for founding the objective
        elif prevState[0][0] == 0 and newState[0][0] != 0:
            reward -= 2
            self.lost = False
            # Resets lost counter
            self.lostCount = 0

        # Reward for centering the objective
        if deltaPosX > 0.05:     # Centering the objective
            reward += 2
        elif deltaPosX < -0.05:  # Threshold
            reward -= 1         # Getting away from the objective

        # Reward for getting closer to the objective
        if (newState[0][0] > (prevState[0][0]+0.01)):
            reward += 2
        elif (newState[0][0] < (prevState[0][0]-0.01)):
            reward -= 1

        # Reward for getting near an obstacle
        laser = newState[2]
        if np.any(laser < (0.5/3)):
            reward -= 60*(0.5/3 - np.min(laser))

        # Update position
        self.pos = self.pepper.getPosition()
        # Verify if robot reach goal
        if (newState[0][0] > 0.99) and (abs(newState[0][1]) < 0.45):
            goalReached = True
            print('SIM '+str(self.ID)+' - EPISODE TERMINATED: Robot reached goal')
            self.endCause[0] += 1
            reward = 100
        # Verify if robot out of bounds
        if (self.pos[0] > MAX_X) or (self.pos[1] > MAX_Y) or (self.pos[0] < MIN_X) or (self.pos[1] < MIN_Y):
            outOfBounds = True
            reward = -75
            print('SIM '+str(self.ID)+' - EPISODE TERMINATED: Robot out of bounds')
            self.endCause[1] += 1
        # Verify if lost for too long
        if self.lostCount > 20:
            lostForTooLong = True
            reward = -75
            print('SIM '+str(self.ID)+' - EPISODE TERMINATED: Robot lost for too long')
            self.endCause[2] += 1
        # Verify if collision with obstacle
        dToObstacle = m.sqrt((self.pos[0] - self.obstacleX)**2 +
                             (self.pos[1] - self.obstacleY)**2)
        # Verify if collision with human
        dToHuman = []
        for i in range(0, 3):
            dToHuman.append(m.sqrt((self.pos[0] - self.humanX[i])**2 +
                                   (self.pos[1] - self.humanY[i])**2))
        minToHuman = np.amin(dToHuman)
        print('Distancia a la persona más cercana: '+str(round(minToHuman, 2)))

        if dToObstacle < 0.35 or minToHuman < 0.35:
            collision = True
            reward = -75
            print('SIM '+str(self.ID)+' - EPISODE TERMINATED: Robot collide with an obstacle')
            self.endCause[3] += 1
        # Verify if max iterations reached
        if (step > self.maxIt):
            maxItReached = True
            reward = - 50
            print('SIM '+str(self.ID)+' - EPISODE TERMINATED: Max iterations reached')
            self.endCause[4] += 1

        # Print final reward
        print('SIM '+str(self.ID)+' - Lost Counter: ' + str(self.lostCount))
        print('SIM '+str(self.ID)+' - (Reward) r: ' + str(reward/100))
        # End episode condition
        endEpisode = maxItReached or outOfBounds or goalReached or lostForTooLong or collision

        return (reward/100), endEpisode

    # ------------------------ Q-Learning ------------------------
    def save_step_in_memory(self, state, action, reward, terminalState):
        # Reshape input state for DQN Model
        state[1] = state[1].reshape(60, 80, 1)
        # Save step in memory
        self.faceMemory.append(state[0])
        self.depthMemory.append(state[1])
        self.laserMemory.append(state[2])
        self.actionMemory.append(action)
        self.rewardMemory.append(reward)
        self.terminalState.append(terminalState)

        if state[0][0]:
            self.lostStates.append(0)
        else:
            self.lostStates.append(1)

        memorySize = len(self.faceMemory)

        # Verify if there is enoguh memory to train
        if memorySize > self.buffer_size:
            # Delete first step of memory
            self.faceMemory.pop(0)
            self.depthMemory.pop(0)
            self.laserMemory.pop(0)
            self.actionMemory.pop(0)
            self.rewardMemory.pop(0)
            self.terminalState.pop(0)
            self.lostStates.pop(0)

        return memorySize

    def update_QModel(self):
        print('Updating DQN Model:')
        print('Selecting random batch...')
        # Uniform probability
        self.pLostState = (1/(len(self.lostStates)))*np.ones(len(self.lostStates))

        # Verify if number of lost states is too high
        pThresh = 0.1
        if np.sum(self.lostStates)/len(self.faceMemory) > pThresh:
            # Reduce probability of selecting lost state to pThresh
            newPLost = pThresh/np.sum(self.lostStates)
            newP = (1-pThresh)/((len(self.lostStates))-np.sum(self.lostStates))
            for i, lost in enumerate(self.lostStates):
                if lost:
                    self.pLostState[i] = newPLost
                else:
                    self.pLostState[i] = newP

        # Retrieving random batch from memory
        randIndex = np.random.choice(len(self.faceMemory), self.batchSize,
                                     replace=False, p=self.pLostState)
        # Saving random batch in aux variables
        faceBatch = []
        depthBatch = []
        laserBatch = []
        nextFaceBatch = []
        nextDepthBatch = []
        nextLaserBatch = []
        actionBatch = []
        rewardBatch = []
        isTerminalState = []
        for i in randIndex:
            # Verify that is not the latest state
            if i >= len(self.faceMemory)-1:
                i = len(self.faceMemory)-2
            # Create batch
            faceBatch.append(self.faceMemory[i])
            depthBatch.append(self.depthMemory[i])
            laserBatch.append(self.laserMemory[i])
            nextFaceBatch.append(self.faceMemory[i+1])
            nextDepthBatch.append(self.depthMemory[i+1])
            nextLaserBatch.append(self.laserMemory[i+1])
            actionBatch.append(self.actionMemory[i])
            rewardBatch.append(self.rewardMemory[i])
            isTerminalState.append(self.terminalState[i])
        # Retrieving original Q-values
        qValues = []
        # nextQValues = []
        print('Retrieving original Q-Values...')

        qValues = self.DQNModel.get_values([faceBatch, depthBatch, laserBatch])
        # nextQValueAction = np.argmax(self.TargetDQNModel.get_values([nextFaceBatch, nextDepthBatch, nextLaserBatch]), axis=1)
        nextQValues = self.DQNModel.get_values([nextFaceBatch, nextDepthBatch, nextLaserBatch])

        print('Calculating target Q-Values...')
        # Calculating target Q Values
        targetQ = np.zeros((self.batchSize, 5))
        for i, action in enumerate(actionBatch):
            qActual = qValues[i]
            # Update original Q-Values with reward
            if isTerminalState[i]:
                qActual[action] = rewardBatch[i]
            else:
                # Double DQN
                # nextQValue = nextQValues[i][nextQValueAction[i]]
                # DQN
                nextQValue = np.max(nextQValues[i])
                qActual[action] = rewardBatch[i] + self.gamma*nextQValue
            targetQ[i] = qActual

        print(targetQ[0])

        # Training DQN Model
        print('Training model with random batch...')
        metrics = self.DQNModel.train([faceBatch, depthBatch, laserBatch], targetQ)

        metrics.append(np.average(targetQ))

        return metrics


if __name__ == "__main__":
    # Pepper Training objects
    pepper_trainner1 = PepperTrainner(min_memory=1000, buffer_size=5000,
                                      batch_size=32, iterations=50,
                                      ID='DQN_DEF', gamma=0.99,
                                      learning_rate=0.0005,
                                      save_info=True, load_training=False,
                                      training_episodes=200)
    # Start training
    pepper_trainner1.main()
