#! /usr/bin/env Python3

import time
import numpy as np
import math as m
import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=FutureWarning)
    import tensorflow.keras
    from tensorflow.keras.models import load_model, model_from_json
    import tensorflow as tf

from dqn_model import DQNModel


def loadModel():
    # Create and save model
    dqnModel = DQNModel(lr=0.1)
    print('Guardando modelo...')
    #dqnModel.save_w('modelTest')

    time.sleep(1)
    print('Cargando modelo...')
    # Recargue el modelo de los 2 archivos que guardamos
    #with open('model_config.json') as json_file:
    #    json_config = json_file.read()
    #print('Convirtiendo modelo...')
    #new_model = model_from_json(json_config)
    print('Cargando pesos...')
    dqnModel.load_w('DQNModel_test1')
    dqnModel.compile(lr=0.01)


if __name__ == "__main__":
	loadModel()
