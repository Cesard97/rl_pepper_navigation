# --------------------------------------------------------------
# ------------------------- Libraries --------------------------
# --------------------------------------------------------------

import warnings
import math as m
import numpy as np
import matplotlib.pyplot as plt

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    from stable_baselines.results_plotter import load_results, ts2xy


def moving_average(values, window):
    """
    Smooth values by doing a moving average
    :param values: (numpy array)
    :param window: (int)
    :return: (numpy array)
    """
    weights = np.repeat(1.0, window) / window
    return np.convolve(values, weights, 'valid')


def tendency(x, n=5):
    size = int(np.floor(len(x)/n))
    xTend = np.zeros(size)
    desvTend = np.zeros(size)
    yTend = np.linspace(n, size*n, size)

    xTend[0] = np.average(x[0:n])
    desvTend[0] = np.std(x[0:n])
    for i in range(1, size):
        xTend[i] = np.average(x[int(yTend[i-1]):int(yTend[i])])
        desvTend[i] = np.std(x[int(yTend[i-1]):int(yTend[i])])

    return xTend, yTend, desvTend/2


def plot_results(log_folder, title='Learning Curve'):
    """
    plot the results

    :param log_folder: (str) the save location of the results to plot
    :param title: (str) the title of the task to plot
    """
    x, y = ts2xy(load_results(log_folder), 'timesteps')
    y = moving_average(y, window=1)
    # Truncate x
    x = x[len(x) - len(y):]

    fig = plt.figure(title)
    plt.plot(x, y)
    plt.xlabel('Number of Timesteps')
    plt.ylabel('Rewards')
    plt.title(title + " Smoothed")
    plt.show()

def plot_results_2(log_folder):
    x, y = ts2xy(load_results(log_folder), 'timesteps')
    print('Total number of steps: ' + str(x[-1]))
    reward = y
    averageReward, x1, desv = tendency(reward, n=20)

    plt.plot(reward, '-', linewidth=0.25)
    plt.plot(x1, averageReward, 'bo-', linewidth=2)
    plt.fill_between(x1, averageReward+desv, averageReward-desv,
                     facecolor='blue', alpha=0.35)
    plt.ylim([-1.5, 1.5])
    plt.ylabel('Acumulated Reward')
    plt.xlabel('Episodes')
    plt.title('Average acumulated reward on training')
    plt.grid(b=True)
    plt.show()


if __name__ == "__main__":
    # Main function
    plot_results_2("/home/cesar/baselines_results/DDQN4/")
    plot_results_2("/home/cesar/baselines_results/DDQN_LR0001/")
    plot_results_2("/home/cesar/baselines_results/DDQN_LR00001/")
