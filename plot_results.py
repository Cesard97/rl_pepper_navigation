#! /usr/bin/env Python3

import time
import numpy as np
import math as m
import matplotlib.pyplot as plt

def moving_average(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

def tendency(x, n=5):
    size = int(np.floor(len(x)/n))
    xTend = np.zeros(size)
    desvTend = np.zeros(size)
    yTend = np.linspace(n, size*n, size)

    xTend[0] = np.average(x[0:n])
    desvTend[0] = np.std(x[0:n])
    for i in range(1,size):
        xTend[i] = np.average(x[int(yTend[i-1]):int(yTend[i])])
        desvTend[i] = np.std(x[int(yTend[i-1]):int(yTend[i])])

    return xTend, yTend, desvTend/2

def plotResults():

    # ----------------------- Plot reward -----------------------
    reward2 = np.loadtxt('results/simpleDQN/G90_acumulated_reward_DQN.csv', delimiter=',')
    reward3 = np.loadtxt('results/simpleDQN/G95_acumulated_reward_DQN.csv', delimiter=',')
    reward4 = np.loadtxt('results/simpleDQN/G99_acumulated_reward_DQN.csv', delimiter=',')

    #reward1 = np.loadtxt('results/DDQN/G90_acumulated_reward_DQN.csv', delimiter=',')
    #reward2 = np.loadtxt('results/DDQN/G95_acumulated_reward_DQN.csv', delimiter=',')
    #reward3 = np.loadtxt('results/DDQN/G99_acumulated_reward_DQN.csv', delimiter=',')

    #reward1 = np.loadtxt('results/DDQN2/lr1_acumulated_reward_DQN.csv', delimiter=',')
    #reward2 = np.loadtxt('results/CDQL_GUI/GUI_acumulated_reward_DQN.csv', delimiter=',')
    #reward3 = np.loadtxt('results/CDQL/lr001_acumulated_reward_DQN.csv', delimiter=',')
    #reward4 = np.loadtxt('results/CDQL/lr0001_acumulated_reward_DQN.csv', delimiter=',')

    #averageReward1, y1, desv1 = tendency(reward1, n=49)
    averageReward2, y2, desv2 = tendency(reward2, n=30)
    averageReward3, y3, desv3 = tendency(reward3, n=30)
    averageReward4, y4, desv4 = tendency(reward4, n=30)

    #lt.plot(reward1, '-', linewidth=1)
    #plt.plot(y1,averageReward1, 'bo-', linewidth=2)
    #plt.legend(['Reward for Episode','Average for 15 Episode'])
    #plt.ylabel('Acumulated reward')
    #plt.xlabel('Episodes')
    #plt.ylim([-1, 1])
    #plt.title('Acumulated reward per episode on training')
    #plt.show()

    #plt.plot(reward1, '-', linewidth=0.25)
    plt.plot(moving_average(reward2, 10), 'b-', linewidth=1.15)
    plt.plot(moving_average(reward3, 10), 'r-', linewidth=1.15)
    plt.plot(moving_average(reward4, 10), 'y-', linewidth=1.15)
    #plt.plot(y1,averageReward1, 'bo-', linewidth=2)
    #plt.fill_between(y1, averageReward1+desv1, averageReward1-desv1, facecolor='blue', alpha=0.35)
    #plt.plot(y2, averageReward2, 'b-', linewidth=2)
    #plt.fill_between(y2, averageReward2+desv2, averageReward2-desv2, facecolor='blue', alpha=0.35)
    #plt.plot(y3, averageReward3, 'r-', linewidth=2)
    #plt.fill_between(y3, averageReward3+desv3, averageReward3-desv3, facecolor='red', alpha=0.35)
    #plt.plot(y4, averageReward4, 'y-', linewidth=2)
    #plt.fill_between(y4, averageReward4+desv4, averageReward4-desv4, facecolor='yellow', alpha=0.35)

    #plt.legend(['gamma = 0.90','gamma = 0.95','gamma = 0.99','Average gamma = 0.90','Average gamma = 0.95','Average gamma = 0.99'])
    plt.legend(['lr = 0.01','lr = 0.001','lr = 0.0001', '50 Episode Average lr = 0.01','50 Episode Average lr = 0.001','50 Episode Average lr = 0.0001'], loc='upper left')
    #plt.legend(['Lineal','Asymptotical','Exponential','Average Lineal','Average Asymptotical','Average Exponential'])
    plt.ylim([-1, 1])
    plt.ylabel('Acumulated reward')
    plt.xlabel('Episodes')
    plt.title('Acumulated reward per episode on training')
    plt.grid(b=True)
    plt.show()

    # ----------------------- Plot termination Cause -----------------------
    cause = ['Goal Reached', 'Out of Bounds', 'Lost for too long', 'Collision', 'Max It Reached']
    #count1 = np.loadtxt('results/simpleDQN/G90_termination_cause_DQN.csv', delimiter=',')
    #count2 = np.loadtxt('results/simpleDQN/G95_termination_cause_DQN.csv', delimiter=',')
    #count3 = np.loadtxt('results/simpleDQN/G99_termination_cause_DQN.csv', delimiter=',')

    #count1 = np.loadtxt('results/DDQN/G90_termination_cause_DQN.csv', delimiter=',')
    #count2 = np.loadtxt('results/DDQN/G95_termination_cause_DQN.csv', delimiter=',')
    #count3 = np.loadtxt('results/DDQN/G99_termination_cause_DQN.csv', delimiter=',')

    #count1 = np.loadtxt('results/DDQN2/lr1_termination_cause_DQN.csv', delimiter=',')
    count2 = np.loadtxt('results/CDQL/lr01_termination_cause_DQN.csv', delimiter=',')
    count3 = np.loadtxt('results/CDQL/lr001_termination_cause_DQN.csv', delimiter=',')
    count4 = np.loadtxt('results/CDQL/lr0001_termination_cause_DQN.csv', delimiter=',')

    x = np.arange(len(cause))  # the label locations
    width = 0.2  # the width of the bars

    fig, ax = plt.subplots()
    #rects1 = ax.bar(x - width, count1, width, label='Gamma = 0.90')
    #rects2 = ax.bar(x, count2, width, label='Gamma = 0.95')
    #rects3 = ax.bar(x + width, count3, width, label='Gamma = 0.99')

    rects2 = ax.bar(x - width, count2, width, label='Lr = 0.01')
    rects3 = ax.bar(x, count3, width, label='Lr = 0.001')
    rects4 = ax.bar(x + width, count4, width, label='Lr = 0.0001')

    #rects1 = ax.bar(x - 3*width/2, count1, width, label='lr = 0.1')
    #rects2 = ax.bar(x - width/2, count2, width, label='lr = 0.01')
    #rects3 = ax.bar(x + width/2, count3, width, label='lr = 0.001')
    #rects4 = ax.bar(x + 3*width/2, count4, width, label='lr = 0.0001')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Number of episodes')
    ax.set_title('Termination cause')
    ax.set_xticks(x)
    ax.set_xticklabels(cause)
    ax.legend()

    def autolabel(rects):
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')

    #autolabel(rects1)
    autolabel(rects2)
    autolabel(rects3)
    autolabel(rects4)
    plt.show()

    # ----------------------- Plot Model Loss -----------------------
    #loss1 = np.loadtxt('results/DDQN/G90_average_loss_DQN.csv', delimiter=',')
    #loss2 = np.loadtxt('results/DDQN/G95_average_loss_DQN.csv', delimiter=',')
    #loss3 = np.loadtxt('results/DDQN/G99_average_loss_DQN.csv', delimiter=',')

    #loss1 = np.loadtxt('results/DDQN2/lr1_average_loss_DQN.csv', delimiter=',')
    loss2 = np.loadtxt('results/CDQL/lr01_average_loss_DQN.csv', delimiter=',')
    loss3 = np.loadtxt('results/CDQL/lr001_average_loss_DQN.csv', delimiter=',')
    loss4 = np.loadtxt('results/CDQL/lr0001_average_loss_DQN.csv', delimiter=',')

    #loss1 = np.loadtxt('results/simpleDQN/G90_average_loss_DQN.csv', delimiter=',')
    #loss2 = np.loadtxt('results/simpleDQN/G95_average_loss_DQN.csv', delimiter=',')
    #loss3 = np.loadtxt('results/simpleDQN/G99_average_loss_DQN.csv', delimiter=',')

    #plt.plot(loss1, '-', linewidth=0.75)
    plt.plot(loss2, '-', linewidth=0.75)
    plt.plot(loss3, '-', linewidth=0.75)
    plt.plot(loss4, '-', linewidth=0.75)

    plt.legend(['lr = 0.1','lr = 0.01','lr = 0.001','lr = 0.0001'])
    #plt.legend(['Gamma = 0.90','Gamma = 0.95','Gamma = 0.99'])
    #plt.legend(['Lineal','Asymptotical','Exponential'])
    plt.ylabel('Average Loss')
    plt.xlabel('Episodes')
    plt.title('Average Loss per episode on training')
    plt.yscale('log')
    plt.grid(b=True)
    plt.show()

    # ----------------------- Plot Model Accuracy -----------------------
    #acc1 = np.loadtxt('results/DDQN/G90_average_accuracy_DQN.csv', delimiter=',')
    #acc2 = np.loadtxt('results/DDQN/G95_average_accuracy_DQN.csv', delimiter=',')
    #acc3 = np.loadtxt('results/DDQN/G99_average_accuracy_DQN.csv', delimiter=',')

    #acc1 = np.loadtxt('results/DDQN2/lr1_average_accuracy_DQN.csv', delimiter=',')
    acc2 = np.loadtxt('results/CDQL/lr01_average_accuracy_DQN.csv', delimiter=',')
    acc3 = np.loadtxt('results/CDQL/lr001_average_accuracy_DQN.csv', delimiter=',')
    acc4 = np.loadtxt('results/CDQL/lr0001_average_accuracy_DQN.csv', delimiter=',')

    #acc1 = np.loadtxt('results/simpleDQN/G90_average_accuracy_DQN.csv', delimiter=',')
    #acc2 = np.loadtxt('results/simpleDQN/G95_average_accuracy_DQN.csv', delimiter=',')
    #acc3 = np.loadtxt('results/simpleDQN/G99_average_accuracy_DQN.csv', delimiter=',')

    #plt.plot(acc1, '-', linewidth=1)
    plt.plot(acc2, '-', linewidth=1)
    plt.plot(acc3, '-', linewidth=1)
    plt.plot(acc4, '-', linewidth=1)

    plt.legend(['lr = 0.1','lr = 0.01','lr = 0.001','lr = 0.0001'])
    #plt.legend(['Lineal','Asymptotical','Exponential'])
    #plt.legend(['Gamma = 0.90','Gamma = 0.95','Gamma = 0.99'])
    plt.ylabel('Average Accuracy')
    plt.xlabel('Episodes')
    plt.title('Average Accuracy per episode on training')
    plt.grid(b=True)
    plt.show()


    # ----------------------- Plot Average Q-Values -----------------------
    #acc1 = np.loadtxt('results/DDQN2/lr1_qValues_average_DQN.csv', delimiter=',')
    qv2 = np.loadtxt('results/simpleDQN/G90_qValues_average_DQN.csv', delimiter=',')
    qv3 = np.loadtxt('results/simpleDQN/G95_qValues_average_DQN.csv', delimiter=',')
    qv4 = np.loadtxt('results/simpleDQN/G99_qValues_average_DQN.csv', delimiter=',')

    #acc1 = np.loadtxt('results/simpleDQN/G90_qValues_average_DQN.csv', delimiter=',')
    #acc2 = np.loadtxt('results/simpleDQN/G95_qValues_average_DQN.csv', delimiter=',')
    #acc3 = np.loadtxt('results/simpleDQN/G99_qValues_average_DQN.csv', delimiter=',')

    #acc1 = np.loadtxt('results/DDQN/G90_qValues_average_DQN.csv', delimiter=',')
    #acc2 = np.loadtxt('results/DDQN/G95_qValues_average_DQN.csv', delimiter=',')
    #acc3 = np.loadtxt('results/DDQN/G99_qValues_average_DQN.csv', delimiter=',')

    #plt.plot(acc1, '-', linewidth=1)
    plt.plot(qv2, '-', linewidth=1)
    plt.plot(qv3, '-', linewidth=1)
    plt.plot(qv4, '-', linewidth=1)

    # plt.legend(['lr = 0.1','lr = 0.01','lr = 0.001','lr = 0.0001'])
    # plt.legend(['Lineal','Asymptotical','Exponential'])
    plt.legend(['Gamma = 0.90', 'Gamma = 0.95', 'Gamma = 0.99'])
    plt.ylabel('Average Q Values')
    plt.xlabel('Episodes')
    plt.title('Average Q Values per episode on training')
    plt.grid(b=True)
    plt.show()


if __name__ == "__main__":
    plotResults()
