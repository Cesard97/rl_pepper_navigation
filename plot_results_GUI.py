#! /usr/bin/env Python3

import time
import numpy as np
import math as m
import matplotlib.pyplot as plt


def moving_average(a, n=3):
    a = a[~np.isnan(a)]
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def tendency(x, n=5):
    size = int(np.floor(len(x)/n))
    xTend = np.zeros(size)
    desvTend = np.zeros(size)
    yTend = np.linspace(n, size*n, size)

    xTend[0] = np.average(x[0:n])
    desvTend[0] = np.std(x[0:n])
    for i in range(1, size):
        xTend[i] = np.average(x[int(yTend[i-1]):int(yTend[i])])
        desvTend[i] = np.std(x[int(yTend[i-1]):int(yTend[i])])

    return xTend, yTend, desvTend/4


def plotResults():
    # ----------------------- Plot reward -----------------------
    reward1 = np.loadtxt('results/Final_Training/DQN_DEF_acumulated_reward_DQN.csv',
                         delimiter=',')

    reward2 = np.loadtxt('results/Final_Training/NEW_CDQL_DEF_acumulated_reward_DQN.csv',
                         delimiter=',')

    reward3 = np.loadtxt('results/Final_Training/DDQN_DEF_acumulated_reward_DQN.csv',
                         delimiter=',')

    averageReward1, y1, desv1 = tendency(reward1, n=50)
    averageReward2, y2, desv2 = tendency(reward2, n=50)

    # averageReward2 = np.array([-0.35, -0.3, -0.15, 0.2, 0.49, 0.51, 0.125, 0.25, 0.5, 0.21, 0.45, 0.55, 0.27])
    # y2 = np.array([50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650])
    # desv2 = np.array([0.25, 0.25, 0.2, 0.22, 0.27, 0.22, 0.27, 0.24, 0.22, 0.24, 0.25, 0.23, 0.22])

    averageReward3, y3, desv3 = tendency(reward3, n=50)

    #plt.plot(reward1, '-', linewidth=0.25)
    #plt.plot(y1, averageReward1, 'b-', linewidth=2)
    #plt.fill_between(y1, averageReward1+desv1, averageReward1-desv1,
    #                 facecolor='blue', alpha=0.35)

    plt.plot(reward2, '-', linewidth=0.25)
    plt.plot(y2, averageReward2, 'b-', linewidth=2)
    plt.fill_between(y2, averageReward2+desv2, averageReward2-desv2,
                     facecolor='blue', alpha=0.35)

    plt.plot(reward3, '-', color='salmon',linewidth=0.25)
    plt.plot(y3, averageReward3, 'r-', linewidth=2)
    plt.fill_between(y3, averageReward3+desv3, averageReward3-desv3,
                     facecolor='red', alpha=0.35)

    plt.legend(['Clipped Double Q-Learning (CDQL)', 'Average reward per 50 Episodes (CDQL)', 'Double Deep Q-Network (DDQN)', 'Average reward per 50 Episodes (DDQN)'], loc='upper left')
    # plt.legend(['Clipped Double Q-Learning (CDQL)', 'Double Deep Q-Network (DDQN)'], loc='upper left')
    # plt.legend(['Deep Q-Networks (DQN)', 'Average per 50 episodes', '1/2 Standard Dev'], loc='upper left')
    plt.ylim([-1.5, 1])
    plt.xlim([0, 500])
    plt.ylabel('Average acumulated reward per episode')
    plt.xlabel('Episodes')
    plt.title('Average acumulated reward on training')
    plt.grid(b=True)
    #plt.xlim([0, 500])
    plt.show()

    # ----------------------- Plot termination Cause -----------------------
    cause = ['Goal Reached', 'Out of Bounds', 'Lost for too long',
             'Collision', 'Max It Reached']

    count1 = np.loadtxt('results/Final_Training/NEW_CDQL_DEF_termination_cause_DQN.csv',
                        delimiter=',')

    count2 = np.loadtxt('results/Final_Training/DDQN_DEF_termination_cause_DQN.csv',
                        delimiter=',')

    x = np.arange(len(cause))  # the label locations
    width = 0.2  # the width of the bars

    fig, ax = plt.subplots()

    # rects1 = ax.bar(x - width, count2, width, label='Lr = 0.01')
    rects1 = ax.bar(x, count1, width, label='CDQL')
    rects2 = ax.bar(x + width, count2, width, label='DDQN')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Number of episodes')
    ax.set_title('Termination cause')
    ax.set_xticks(x)
    ax.set_xticklabels(cause)
    ax.legend()

    def autolabel(rects):
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')

    autolabel(rects1)
    autolabel(rects2)
    # autolabel(rects3)
    # autolabel(rects4)
    plt.show()

    # ----------------------- Plot Model Loss -----------------------

    loss1 = np.loadtxt('results/Final_Training/NEW_CDQL_DEF_average_loss_DQN.csv', delimiter=',')
    loss2 = np.loadtxt('results/Final_Training/DDQN_DEF_average_loss_DQN.csv', delimiter=',')
    # loss2 = np.loadtxt('results/CDQL/lr001_average_loss_DQN.csv', delimiter=',')
    # loss3 = np.loadtxt('results/CDQL/lr0001_average_loss_DQN.csv', delimiter=',')

    plt.plot(loss1, '-', linewidth=0.75)
    plt.plot(loss2, '-', linewidth=0.75)
    # plt.plot(loss3, '-', linewidth=0.75)

    plt.legend(['CDQL', 'DDQN'])
    plt.ylabel('Average Loss')
    plt.xlabel('Episodes')
    plt.title('Average Loss per episode on training')
    plt.yscale('log')
    plt.xlim([0, 500])
    plt.grid(b=True)
    plt.show()

    # ----------------------- Plot Model Accuracy -----------------------

    acc1 = np.loadtxt('results/Final_Training/NEW_CDQL_DEF_average_accuracy_DQN.csv', delimiter=',')
    acc2 = np.loadtxt('results/Final_Training/DDQN_DEF_average_accuracy_DQN.csv', delimiter=',')
    # acc2 = np.loadtxt('results/CDQL/GUI_average_accuracy_DQN.csv', delimiter=',')
    # acc3 = np.loadtxt('results/CDQL/lr001_average_accuracy_DQN.csv', delimiter=',')

    plt.plot(acc1, '-', linewidth=0.75)
    plt.plot(acc2, '-', linewidth=0.75)
    # plt.plot(acc3, '-', linewidth=0.75)

    plt.legend(['CDQL', 'DDQN'])
    plt.ylabel('Average Accuracy')
    plt.xlabel('Episodes')
    plt.title('Average Accuracy per episode on training')
    plt.grid(b=True)
    plt.xlim([0, 500])
    plt.show()

    # ----------------------- Plot Average Q-Values -----------------------
    qv1 = np.loadtxt('results/Final_Training/NEW_CDQL_DEF_qValues_average_DQN.csv', delimiter=',')
    qv2 = np.loadtxt('results/Final_Training/DDQN_DEF_qValues_average_DQN.csv', delimiter=',')
    # qv2 = np.loadtxt('results/CDQL/lr001_qValues_average_DQN.csv', delimiter=',')
    # qv3 = np.loadtxt('results/CDQL/lr0001_qValues_average_DQN.csv', delimiter=',')

    plt.plot(qv1, 'b-', linewidth=0.75)
    plt.plot(qv2, 'r-', linewidth=0.75)
    # plt.plot(qv3, '-', linewidth=0.75)
    # plt.legend(['DQN', 'DDQN'])
    plt.legend(['CDQL', 'DDQN'])
    plt.ylabel('Average Q Values')
    plt.xlabel('Episodes')
    plt.title('Average Q Values per episode on training')
    plt.grid(b=True)
    plt.xlim([0, 500])
    plt.show()


def plot_results_test():
    score = np.loadtxt('resultsTest/CDQL_TEST_reward_test.csv', delimiter=',')
    #score = np.append(score, np.array([1, 1]).reshape(1,2), axis=0)
    print(score)
    fig, ax = plt.subplots()
    ax.matshow(score, cmap=plt.cm.RdYlBu)
    for i in range(9):
        for j in range(4):
            c = round(score[j, i], 2)
            ax.text(i, j, str(c), c='white', size='x-large', va='center', ha='center')

    plt.show()


def plot_results_all_test():
    cause = ['CDQL', 'CDQL in New Map', 'DDQN', 'DDQN in New Map']

    count1 = [0.81, 0.80, 0.26, 0.40]
    count2 = [0.02, 0.16, 0.31, 0.20]
    count3 = [0.18, 0.04, 0.43, 0.40]

    x = np.arange(len(cause))  # the label locations
    width = 0.2  # the width of the bars

    fig, ax = plt.subplots()

    rects1 = ax.bar(x - width, count1, width, label='Reached goal')
    rects2 = ax.bar(x, count2, width, label='Collision with obstacle')
    rects3 = ax.bar(x + width, count3, width, label='Max iterations reached')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('% of Episodes')
    ax.set_title('Termination Cause in Test Episodes')
    ax.set_xticks(x)
    ax.set_xticklabels(cause)
    ax.legend()
    plt.ylim([0, 1])

    def autolabel(rects):
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')

    autolabel(rects1)
    autolabel(rects2)
    autolabel(rects3)
    # autolabel(rects4)
    plt.show()


if __name__ == "__main__":
    # plotResults()
    plot_results_test()
    # plot_results_all_test()
