# --------------------------------------------------------------
# ------------------------- Libraries --------------------------
# --------------------------------------------------------------
import time
import os
import warnings
import math as m
import random
import cv2
import pybullet as pb
import pybullet_data
import numpy as np
import gym
import matplotlib.pyplot as plt

from gym import spaces
from qibullet import PepperVirtual
from qibullet import SimulationManager

from pepper_env import PepperSimEnv, SaveOnBestTrainingRewardCallback

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    from stable_baselines.deepq.policies import CnnPolicy
    from stable_baselines.common.policies import CnnLstmPolicy
    from stable_baselines import DQN, A2C, PPO2
    from stable_baselines.bench import Monitor

# --------------------------------------------------------------
# ------------------------- Constants --------------------------
# --------------------------------------------------------------
# Physical Boundaries
MAX_X = 5.7
MIN_X = -3.7
MAX_Y = 5.7
MIN_Y = -5.7


def train_dqn():
    # Create log dir
    log_dir = "/home/cesar/baselines_results/NEW_DDQN_LR00001/"
    os.makedirs(log_dir, exist_ok=True)
    # Pepper Training Env
    env = PepperSimEnv()
    env = Monitor(env, log_dir)
    # Create the callback: check every 100 steps
    callback = SaveOnBestTrainingRewardCallback(check_freq=500, log_dir=log_dir)
    # Model
    cnn_model = DQN(CnnPolicy, env, buffer_size=10000, exploration_fraction=0.2,
                    exploration_final_eps=0.05, exploration_initial_eps=1.0,
                    train_freq=1, batch_size=32, double_q=True,
                    learning_starts=1000, target_network_update_freq=500,
                    prioritized_replay=True, learning_rate=0.00001, verbose=1)
    # Training
    cnn_model.learn(total_timesteps=20000, callback=callback)


def train_a2c():
    # Create log dir
    log_dir = "/home/cesar/baselines_results/A2C/"
    os.makedirs(log_dir, exist_ok=True)
    # Pepper Training Env
    env = PepperSimEnv()
    env = Monitor(env, log_dir)
    # Create the callback: check every 100 steps
    callback = SaveOnBestTrainingRewardCallback(check_freq=99, log_dir=log_dir)
    # Model
    model = A2C(CnnLstmPolicy, env, verbose=1)
    # Training
    model.learn(total_timesteps=50000, callback=callback)


def train_ppo():
    # Create log dir
    log_dir = "/home/cesar/baselines_results/PPO/"
    os.makedirs(log_dir, exist_ok=True)
    # Pepper Training Env
    env = PepperSimEnv()
    env = Monitor(env, log_dir)
    # Create the callback: check every 100 steps
    callback = SaveOnBestTrainingRewardCallback(check_freq=1250, log_dir=log_dir)
    # Model
    model = PPO2(CnnLstmPolicy, env, verbose=1)
    # Training
    model.learn(total_timesteps=50000, callback=callback)


if __name__ == "__main__":
    # Main function
    train_dqn()
    # train_a2c()
    # train_ppo()
