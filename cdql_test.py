#! /usr/bin/env Python3

# --------------------------------------------------------------
# ------------------------- Libraries --------------------------
# --------------------------------------------------------------
import warnings
import time
import cv2
import random
import threading
import pybullet as pb
import pybullet_data
import numpy as np
import math as m
import matplotlib.pyplot as plt
from qibullet import PepperVirtual
from qibullet import SimulationManager
from dqn_model import DQNModel

# --------------------------------------------------------------
# ------------------------- Constants --------------------------
# --------------------------------------------------------------
# Physical Boundaries
MAX_X = 5.6
MIN_X = -3.6
MAX_Y = 5.6
MIN_Y = -5.6


class PepperTest():
    # --------------------------------------------------------------
    # ----------------------- Initialization -----------------------
    # --------------------------------------------------------------
    def __init__(self, ID=0, save_results=False, iterations=80):
        # Parameters
        self.ID = ID
        self.save_results = save_results
        self.maxIt = iterations
        # Initial Pos
        self.pos = [0, 0, 0]
        self.obstacleX = 100
        self.obstacleY = 100
        self.lost = False
        self.lostCount = 0
        # Q-Learning Model
        self.DQNModel1 = DQNModel()  # Load trained model
        self.DQNModel2 = DQNModel()  # Load trained model
        # Load trained model
        self.DQNModel1.load_w('DQNModel1_CDQL_DEF')
        self.DQNModel1.compile(lr=0.01)

        self.DQNModel2.load_w('DQNModel2_CDQL_DEF')
        self.DQNModel2.compile(lr=0.01)

        self.acumulatedReward = []
        self.endCause = [0, 0, 0, 0, 0]
        self.averageLoss = []
        self.averageAccuracy = []
        # Step Memory
        self.step = 0
        self.episode = 0
        self.score = np.zeros((8, 2))
        # Simulation
        self.simulation_manager = SimulationManager()
        # Human Positions
        self.humanX = np.ones(3)
        self.humanY = np.ones(3)
        self.obstacle_prob = 0

    # Configuration of the simulation environment
    def set_environment(self, human_x, human_y, init_pos=0):
        print('Setting a new simulation environment...')
        # Client number
        self.client = self.simulation_manager.launchSimulation(gui=True)
        # Create and spawn Pepper in a random position
        if init_pos == 0:
            self.pepper = self.simulation_manager.spawnPepper(
                          self.client, spawn_ground_plane=True,
                          translation=[0, 0, 0])

        if init_pos == 1:
            self.pepper = self.simulation_manager.spawnPepper(
                          self.client, spawn_ground_plane=True,
                          translation=[3, 4, 0],
                          quaternion=[0.0, 0.0, -m.pi/2, m.pi/2])

        if init_pos == 2:
            self.pepper = self.simulation_manager.spawnPepper(
                          self.client, spawn_ground_plane=True,
                          translation=[3, -4, 0],
                          quaternion=[0.0, 0.0, -m.pi/2, -m.pi/2])

        self.simulation_manager.setLightPosition(self.client, [0, 0, 1.0])
        # Subscribe to lasers
        self.pepper.showLaser(True)
        self.pepper.subscribeLaser()
        # Creating enviroment
        pb.setAdditionalSearchPath(pybullet_data.getDataPath())
        self.simulation_manager.setLightPosition(self.client, [0, 0, 1.0])
        # Spawn walls
        print('Spawning world')
        pb.loadURDF('wallX.urdf', basePosition=[1, 6.1, 0])
        pb.loadURDF('wallX.urdf', basePosition=[1, -6.1, 0])

        pb.loadURDF('wallY.urdf', basePosition=[-4.1, 0, 0])
        pb.loadURDF('wallY.urdf', basePosition=[6.1, 0, 0])

        # Spawn 3 human randomly
        print('Spawning humans...')

        self.humanX[0] = human_x
        self.humanY[0] = human_y
        pb.loadURDF('humanoid/humanoid.urdf',
                    basePosition=[human_x, human_y, 0.85],
                    baseOrientation=[0.0, m.pi/2, m.pi/2, 0.0],
                    globalScaling=0.225, useFixedBase=1)

        self.humanX[1] = -3.6
        self.humanY[1] = -5.6
        #pb.loadURDF('humanoid/humanoid.urdf',
        #            basePosition=[self.humanX[1], self.humanY[1], 0.85],
        #            baseOrientation=[m.pi/2, m.pi/2, m.pi/2, m.pi/2],
        #            globalScaling=0.225, useFixedBase=1)

        self.humanX[2] = -3.6
        self.humanY[2] = 5.6
        #pb.loadURDF('humanoid/humanoid.urdf',
        #            basePosition=[self.humanX[2], self.humanY[2], 0.85],
        #            baseOrientation=[-m.pi/2, m.pi/2, m.pi/2, -m.pi/2],
        #            globalScaling=0.225, useFixedBase=1)

        # Spawn obstacle randomly
        print('Spawning obstacles with probability ' + str(self.obstacle_prob))
        dToObjective = m.sqrt(self.humanX[0]**2 + self.humanY[0]**2)
        if (dToObjective > 3.5) and (random.random() < self.obstacle_prob):
            self.obstacleX = self.humanX[0]/2 + random.random() - 0.5
            self.obstacleY = self.humanY[0]/2 + random.random() - 0.5
            obstacleTheta = random.random()*2*m.pi
            pb.loadURDF(
                'chair_1/chair.urdf',
                basePosition=[self.obstacleX, self.obstacleY, 0.0],
                baseOrientation=[0.0, 0.0, m.pi/2, obstacleTheta],
                globalScaling=1.0, useFixedBase=1)
        time.sleep(0.1)

    # Main
    def main(self):
        print('SARTING SIM ' + str(self.ID) + ' !!! ')
        # --------------------------------------------------------------
        # --------------------- Test Double Loop -----------------------
        # --------------------------------------------------------------
        xPos = np.linspace(3, 4, 2)
        yPos = np.linspace(2, -2, 3)
        for i, x in enumerate(xPos):
            for j, y in enumerate(yPos):
                # Start simulation environment
                self.set_environment(x, y)
                # ----------------------- Episode Init ------------------------
                print('--------------------------------------------')
                print('         SIM '+str(self.ID)+': EPISODE ' + str(self.episode))
                print('--------------------------------------------')
                # Restart simulation variables
                step = 0
                totalScore = 0
                endEpisode = False

                # Update initial state
                newState = self.update_state()

                # --------------------------------------------------------------
                # ----------------------- Episode Loop ------------------------
                # --------------------------------------------------------------
                while not endEpisode:
                    # Epsilon-greedy movement
                    action = self.action_selection(newState, 0.01)
                    self.move(action)

                    # Update state
                    prevState = newState
                    newState = self.update_state()

                    # Get reward and endEpisode condition
                    reward, endEpisode = self.get_reward(prevState, newState, step)
                    # Save total episode reward for the episode
                    totalScore += reward

                    # Update step
                    step += 1
                    print('SIM '+str(self.ID)+' - Episode ' + str(self.episode))
                    print('SIM '+str(self.ID)+' - Episode step: ' +str(step))

                # Results from episode
                self.episode += 1
                self.lostCount = 0
                # Score
                self.score[i][j] = totalScore
                print('Acumulated Score: ' + str(self.score))
                # Saving checkpoint from episode
                if self.save_results:
                    print('Saving results from episode...')
                    np.savetxt('resultsTest/'+str(self.ID)+'_reward_test.csv', self.score, delimiter=',')
                    np.savetxt('resultsTest/'+str(self.ID)+'_termination_cause_test.csv', self.endCause, delimiter=',')
                    print('Results were saved')
                # Reset episode
                self.simulation_manager.resetSimulation(self.client)
                self.simulation_manager.stopSimulation(self.client)

    # ---------------------------------------------------------
    # ----------------------- Functions -----------------------
    # ---------------------------------------------------------

    #  ----------------------- ACTION  ------------------------
    # Transforming action to movementresultsNewGammaresultsNewGamma
    def movement_action(self, action=0, vel=0.6):
        if action == 0:
            self.pepper.move(vel, 0.0, 0.0)
        elif action == 1:
            self.pepper.move(0.0, vel, 0.0)
        elif action == 2:
            self.pepper.move(0.0, -vel, 0.0)
        elif action == 3:
            self.pepper.move(0.0, 0.0, m.pi/2)
        elif action == 4:
            self.pepper.move(0.0, 0.0, -m.pi/2)

    # Low level control
    def move(self, action):
        # Get previous position
        prevX, prevY, prevW = self.pepper.getPosition()
        # Move
        print('(Action) Pepper is moving: ' + str(action))
        self.movement_action(action)
        time.sleep(0.5)
        # Update position
        self.pos = self.pepper.getPosition()
        start_moving = time.time()
        # Low level control to verify movement
        while ((abs(prevX-self.pos[0]) < 0.3) and
                (abs(prevY-self.pos[1]) < 0.3) and
                (abs(prevW-self.pos[2]) < m.pi/4)) and ((time.time() - start_moving) < 1.5):
            # Retry pepper movement
            # print('Spaming Pepper movement')
            self.movement_action(action)
            self.pos = self.pepper.getPosition()
            time.sleep(0.001)

        # Stop Pepper movement
        self.movement_action(0, 0)
        prevX, prevY, prevW = self.pepper.getPosition()
        time.sleep(0.1)
        # Update position
        self.pos = self.pepper.getPosition()
        start_stoping = time.time()
        # Low level control to verify stop
        while ((abs(prevX-self.pos[0]) > 0.001) or
               (abs(prevY-self.pos[1]) > 0.001) or
               (abs(prevW-self.pos[2]) > 0.001)) and ((time.time() - start_stoping) < 0.5):

            # print('Spaming Pepper STOP')
            self.movement_action(0, 0)
            prevX, prevY, prevW = self.pepper.getPosition()
            time.sleep(0.1)
            self.pos = self.pepper.getPosition()
        # Print position
        print('Pepper stopped moving: ' + str(round(self.pos[0], 1)) + ', ' + str(round(self.pos[1], 1)) + ', ' + str(round(self.pos[2], 1)))

    # Greedy action selection
    def action_selection(self, state, epsilon=0.1):
        action = 0
        if random.random() < epsilon:
            action = np.random.randint(5)
        else:
            qValues1 = self.DQNModel1.get_values([state[0].reshape(-1, 2), state[1].reshape(-1, 60, 80, 1), state[2].reshape(-1, 45)])
            qValues2 = self.DQNModel1.get_values([state[0].reshape(-1, 2), state[1].reshape(-1, 60, 80, 1), state[2].reshape(-1, 45)])
            qValues = qValues1 + qValues2
            print(qValues/2)
            action = np.argmax(qValues)
        return action

    #  ------------------------ STATE  ------------------------
    def update_state(self):
        state = []
        # Get image and mask for the red sphere
        mask = self.get_mask()
        # Get 3D image from the 3D camera
        depth = self.get_depth()
        # Get lasers
        lasers = self.get_laser()
        # Determine the number of pixels in the mask and normalize
        size = (np.count_nonzero(mask))/1700
        # Minimun threshold
        if size > 0.05:
            # Determine position of the square and normalize betweeb -1 and 1
            posX = np.average(np.nonzero(mask)[1])/160 - 1
            # Return state
            state.append(np.array([size, posX]))
            state.append(depth)
            state.append(lasers)
            print('SIM '+str(self.ID)+' - (State) Area: ' +str(round(size,2)) + ', PosX: ' + str(round(posX,2)))
            return state
        else:
            # Return if lost
            state.append(np.array([0, -1]))
            state.append(depth)
            state.append(lasers)
            print('SIM '+str(self.ID)+' - (State) LOST')
            return state

    # Get mask
    def get_mask(self):
        # Subscribing to the bottom RGB camera
        self.pepper.subscribeCamera(PepperVirtual.ID_CAMERA_TOP)
        # Retrieving and displaying the synthetic image using OpenCV
        img = self.pepper.getCameraFrame()
        # cv2.imshow("Synthetic top camera", img)
        # cv2.waitKey(500)
        # Adding a mask to image to get red pixels
        lower = np.array([0, 0, 100])
        upper = np.array([150, 150, 255])
        mask = cv2.inRange(img,lower,upper)
        # cv2.imshow("Synthetic top camera mask", mask)
        # cv2.waitKey(500)
        self.pepper.unsubscribeCamera(PepperVirtual.ID_CAMERA_TOP)
        return mask

    # Get Depth Camera frame
    def get_depth(self):
        # Subscribing to the bottom RGB camera
        self.pepper.subscribeCamera(PepperVirtual.ID_CAMERA_DEPTH)
        # Retrieving and displaying the synthetic image using OpenCV
        img = self.pepper.getCameraFrame()
        img = cv2.blur(img, (5, 5))
        # Resize image
        img = cv2.resize(img, (80, 60), interpolation=cv2.INTER_AREA)
        # cv2.imshow("Syntehtic depth camera", img)
        # cv2.waitKey(500)
        self.pepper.unsubscribeCamera(PepperVirtual.ID_CAMERA_DEPTH)

        return img

    # Get laser information
    def get_laser(self):
        mergedLaser = []
        mergedLaser = np.array(self.pepper.getRightLaserValue(), dtype=np.float32)
        mergedLaser = np.append(mergedLaser, np.array(self.pepper.getFrontLaserValue(), dtype=np.float32))
        mergedLaser = np.append(mergedLaser, np.array(self.pepper.getLeftLaserValue(), dtype=np.float32))

        return mergedLaser/3

    #  ------------------------ REWARD  ------------------------
    def get_reward(self, prevState, newState, step):
        # Variables
        reward = 0

        goalReached = False
        outOfBounds = False
        lostForTooLong = False
        collision = False
        maxItReached = False

        # Change in horizontal position
        deltaPosX = abs(prevState[0][1]) - abs(newState[0][1])

        # Reward for not changing state
        if all(newState[0] == prevState[0]):
            # Counter if lost
            if self.lost:
                self.lostCount += 1
        # Reward for getting lost
        elif prevState[0][0] != 0 and newState[0][0] == 0:
            self.lost = True
        # Reward for founding the objective
        elif prevState[0][0] == 0 and newState[0][0] != 0:
            self.lost = False
            # Resets lost counter
            self.lostCount = 0

        # Reward for centering the objective
        if deltaPosX > 0.05:     # Centering the objective
            reward += 1
        elif deltaPosX < -0.05:  # Threshold
            reward += -2         # Getting away from the objective

        # Reward for getting closer to the objective
        if (newState[0][0] > (prevState[0][0]+0.01)):
            reward += 1
        elif (newState[0][0] < (prevState[0][0]-0.01)):
            reward += -2

        # Reward for getting near an obstacle
        laser = newState[2]
        if np.any(laser < 0.5/3):
            reward -= 60*(0.5/3 - np.min(laser))

        # Update position
        self.pos = self.pepper.getPosition()
        # Verify if robot reach goal
        if (newState[0][0] > 0.99) and (abs(newState[0][1]) < 0.5):
            goalReached = True
            print('SIM '+str(self.ID)+' - EPISODE TERMINATED: Robot reached goal')
            self.endCause[0] += 1
            reward = 100 - step
        # Verify if robot out of bounds
        if (self.pos[0] > MAX_X) or (self.pos[1] > MAX_Y) or (self.pos[0] < MIN_X) or (self.pos[1] < MIN_Y):
            outOfBounds = True
            reward = -75
            print('SIM '+str(self.ID)+' - EPISODE TERMINATED: Robot out of bounds')
            self.endCause[1] += 1
        # Verify if lost for too long
        if self.lostCount > 80:
            lostForTooLong = True
            reward = -75
            print('SIM '+str(self.ID)+' - EPISODE TERMINATED: Robot lost for too long')
            self.endCause[2] += 1
        # Verify if collision with obstacle
        dToObstacle = m.sqrt((self.pos[0] - self.obstacleX)**2 +
                             (self.pos[1] - self.obstacleY)**2)
        # Verify if collision with human
        dToHuman = []
        for i in range(0, 3):
            dToHuman.append(m.sqrt((self.pos[0] - self.humanX[i])**2 +
                                   (self.pos[1] - self.humanY[i])**2))
        minToHuman = np.amin(dToHuman)
        print('Distancia a la persona más cercana: '+str(round(minToHuman, 2)))

        if dToObstacle < 0.35 or minToHuman < 0.35:
            collision = True
            reward = -75
            print('SIM '+str(self.ID)+' - EPISODE TERMINATED: Robot collide with an obstacle')
            self.endCause[3] += 1
        # Verify if max iterations reached
        if (step > self.maxIt):
            maxItReached = True
            reward = -75
            print('SIM '+str(self.ID)+' - EPISODE TERMINATED: Max iterations reached')
            self.endCause[4] += 1

        # Print final reward
        print('SIM '+str(self.ID)+' - Lost Counter: ' + str(self.lostCount))
        print('SIM '+str(self.ID)+' - (Reward) r: ' + str(reward/100))
        # End episode condition
        endEpisode = maxItReached or outOfBounds or goalReached or lostForTooLong or collision

        return (reward/100), endEpisode


if __name__ == "__main__":

    # Pepper policy test
    pepper_test = PepperTest(iterations=80, ID='CDQL_TEST', save_results=False)
    pepper_test.main()
