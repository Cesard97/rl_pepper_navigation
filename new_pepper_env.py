# --------------------------------------------------------------
# ------------------------- Libraries --------------------------
# --------------------------------------------------------------
import time
import os
import warnings
import math as m
import random
import cv2
import pybullet as pb
import pybullet_data
import numpy as np
import gym
import matplotlib.pyplot as plt

from gym import spaces
from qibullet import PepperVirtual
from qibullet import SimulationManager

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    from stable_baselines.common.env_checker import check_env
    from stable_baselines.deepq.policies import CnnPolicy
    from stable_baselines import DQN, results_plotter
    from stable_baselines.common.callbacks import BaseCallback
    from stable_baselines.bench import Monitor
    from stable_baselines.results_plotter import load_results, ts2xy

# --------------------------------------------------------------
# ------------------------- Constants --------------------------
# --------------------------------------------------------------
# Physical Boundaries
MAX_X = 5.7
MIN_X = -3.7
MAX_Y = 5.7
MIN_Y = -3.7


class SaveOnBestTrainingRewardCallback(BaseCallback):

    def __init__(self, check_freq: int, log_dir: str, verbose=1):
        super(SaveOnBestTrainingRewardCallback, self).__init__(verbose)
        self.check_freq = check_freq
        self.log_dir = log_dir
        self.save_path = os.path.join(log_dir, 'best_model')
        self.best_mean_reward = -np.inf

    def _init_callback(self) -> None:
        # Create folder if needed
        if self.save_path is not None:
            os.makedirs(self.save_path, exist_ok=True)

    def _on_step(self) -> bool:
        if self.n_calls % self.check_freq == 0:

            # Retrieve training reward
            x, y = ts2xy(load_results(self.log_dir), 'timesteps')
            if len(x) > 0:
                # Mean training reward over the last 100 episodes
                mean_reward = np.mean(y[-50:])
                if self.verbose > 0:
                    print("Num timesteps: {}".format(self.num_timesteps))
                    print("Best mean reward: {:.2f} - Last mean reward per episode: {:.2f}".format(self.best_mean_reward, mean_reward))

            # New best model, you could save the agent here
            if mean_reward > self.best_mean_reward:
                self.best_mean_reward = mean_reward
                # Example for saving best model
                if self.verbose > 0:
                    print("Saving new best model to {}".format(self.save_path))
                self.model.save(self.save_path)

        return True


class PepperSimEnv(gym.Env):
    # --------------------------------------------------------------
    # ----------------------- Initialization -----------------------
    # --------------------------------------------------------------

    def __init__(self, max_it=50, max_lost=20, obstacle_prob=0):
        super(PepperSimEnv, self).__init__()
        # Initial Position
        self.pos = [0, 0, 0]
        # Obstacle pos
        self.obstacleX = 100
        self.obstacleY = 100
        # Human Positions
        self.humanX = np.ones(2)
        self.humanY = np.ones(2)
        # States
        self.new_state = None
        self.prev_state = None
        self.lost = False
        self.lostCount = 0
        self.obs = np.zeros((3, 60, 80))
        # Sim Parameters
        self.max_it = max_it
        self.max_lost = max_lost
        self.obstacle_prob = obstacle_prob
        # Sim Manager
        self.simulation_manager = SimulationManager()
        self.client = None
        self.episode_step = 0
        self.reset(True)
        # Action Space
        n_actions = 5
        self.action_space = spaces.Discrete(n_actions)
        # Observation Space
        self.observation_space = spaces.Box(
            low=np.zeros((60, 80, 3)), high=np.ones((60, 80, 3)),
            shape=(60, 80, 3), dtype=np.float32)

    # --------------------------------------------------------------
    # --------------------------- Reset ----------------------------
    # --------------------------------------------------------------

    def reset(self, firstSim=False):
        # Stop and reset simulation
        print('Stopping and reseting simulation')
        if not firstSim:
            self.simulation_manager.resetSimulation(self.client)
            self.simulation_manager.stopSimulation(self.client)

        print('Setting a new simulation environment...')
        # Client number
        self.client = self.simulation_manager.launchSimulation(gui=True)
        # Create and spawn Pepper in a random position
        init_pos = random.randint(0, 2)
        if init_pos == 0:
            self.pepper = self.simulation_manager.spawnPepper(
                          self.client, spawn_ground_plane=True,
                          translation=[0, 0, 0])

        if init_pos == 1:
            self.pepper = self.simulation_manager.spawnPepper(
                          self.client, spawn_ground_plane=True,
                          translation=[4, 4, 0],
                          quaternion=[0.0, 0.0, -m.pi/2, m.pi/2])

        if init_pos == 2:
            self.pepper = self.simulation_manager.spawnPepper(
                          self.client, spawn_ground_plane=True,
                          translation=[-2, -2, 0],
                          quaternion=[0.0, 0.0, -m.pi/2, -m.pi/2])
        # Subscribe to lasers
        self.pepper.showLaser(True)
        self.pepper.subscribeLaser()
        # Reset variables
        self.lostCount = 0
        self.lost = False
        self.episode_step = 0
        # Creating enviroment
        pb.setAdditionalSearchPath(pybullet_data.getDataPath())
        self.simulation_manager.setLightPosition(self.client, [0, 0, 1.0])
        # Spawn walls
        print('Spawning world')
        pb.loadURDF('wallX.urdf', basePosition=[1, 6.1, 0])
        pb.loadURDF('wallX.urdf', basePosition=[1, -4.1, 0])

        pb.loadURDF('wallY.urdf', basePosition=[-4.1, 1, 0])
        pb.loadURDF('wallY.urdf', basePosition=[6.1, 1, 0])

        pb.loadURDF('wall2.urdf', basePosition=[0, -3, 0])
        pb.loadURDF('wall4.urdf', basePosition=[0, 4, 0])

        # Spawn 2 human randomly
        print('Spawning humans')
        self.humanX[0] = random.random()*3.5 + 2
        self.humanY[0] = random.random()*4 - 2
        pb.loadURDF('humanoid/humanoid.urdf',
                    basePosition=[self.humanX[0], self.humanY[0], 0.85],
                    baseOrientation=[0.0, m.pi/2, m.pi/2, 0.0],
                    globalScaling=0.225, useFixedBase=1)
        # self.humanX[1] = random.random()*4 - 3.7
        # self.humanY[1] = random.random()*4 - 5.7
        # pb.loadURDF('humanoid/humanoid.urdf',
        #            basePosition=[self.humanX[1], self.humanY[1], 0.85],
        #            baseOrientation=[m.pi/2, m.pi/2, m.pi/2, m.pi/2],
        #            globalScaling=0.225, useFixedBase=1)
        self.humanX[1] = random.random()*3 - 3.5
        self.humanY[1] = random.random()*3.5 + 2
        pb.loadURDF('humanoid/humanoid.urdf',
                    basePosition=[self.humanX[1], self.humanY[1], 0.85],
                    baseOrientation=[-m.pi/2, m.pi/2, m.pi/2, -m.pi/2],
                    globalScaling=0.225, useFixedBase=1)
        # Spawn obstacle randomly
        print('Spawning obstacles with probability ' + str(self.obstacle_prob))
        dToObjective = m.sqrt(self.humanX[0]**2 + self.humanY[0]**2)
        if (dToObjective > 3.5) and (random.random() < self.obstacle_prob):
            self.obstacleX = self.humanX[0]/2 + random.random() - 0.5
            self.obstacleY = self.humanY[0]/2 + random.random() - 0.5
            obstacleTheta = random.random()*2*m.pi
            pb.loadURDF(
                'chair_1/chair.urdf',
                basePosition=[self.obstacleX, self.obstacleY, 0.0],
                baseOrientation=[0.0, 0.0, m.pi/2, obstacleTheta],
                globalScaling=1.0, useFixedBase=1)
        time.sleep(0.2)
        print('Simulation ' + str(self.client) + ': Ready')

        # Update state
        self.new_state = self.update_state()
        # Observation
        self.obs[0] = self.get_depth()
        self.obs[1] = cv2.resize(self.get_mask(), (80, 60), interpolation=cv2.INTER_AREA)
        self.obs[2] = self.get_local_map()

        return self.obs.reshape(60, 80, 3)

    # --------------------------------------------------------------
    # ---------------------------- Step ----------------------------
    # --------------------------------------------------------------
    def step(self, action):
        print('--------------------------')
        print('Episode Step: ' + str(self.episode_step))
        # Pepper movement
        self.move(action)
        # Save previous state
        self.prev_state = self.new_state
        # Updated new state
        self.new_state = self.update_state()
        # Get reward and endEpisode flag
        reward, endEpisode = self.get_reward(
            self.prev_state, self.new_state, self.episode_step)
        # Next step
        self.episode_step += 1
        # Observation
        self.obs[0] = self.get_depth()
        self.obs[1] = cv2.resize(self.get_mask(), (80, 60), interpolation=cv2.INTER_AREA)
        self.obs[2] = self.get_local_map()

        # Aditional info
        info = {}

        return self.obs.reshape(60, 80, 3), reward, endEpisode, info

    # --------------------------------------------------------------
    # --------------------------- Render ---------------------------
    # --------------------------------------------------------------

    def render(self, mode='console'):
        if mode != 'console':
            raise NotImplementedError()
        # agent is represented as a cross, rest as a dot
        print(
            'Pepper Position: ' + str(round(self.pos[0], 1)) + ', ' +
            str(round(self.pos[1], 1)) + ', ' + str(round(self.pos[2], 1)))

    def close(self):
        pass

    # ---------------------------------------------------------
    # ----------------------- Methods -----------------------
    # ---------------------------------------------------------

    #  ----------------------- ACTION  ------------------------
    # Transforming action to movement
    def movement_action(self, action=0, vel=0.6):
        if action == 0:
            self.pepper.move(vel, 0.0, 0.0)
        elif action == 1:
            self.pepper.move(0.0, vel, 0.0)
        elif action == 2:
            self.pepper.move(0.0, -vel, 0.0)
        elif action == 3:
            self.pepper.move(0.0, 0.0, m.pi/2)
        elif action == 4:
            self.pepper.move(0.0, 0.0, -m.pi/2)

    # Low level control
    def move(self, action):
        # Get previous position
        prevX, prevY, prevW = self.pepper.getPosition()
        # Move
        print('(Action) Pepper is moving: ' + str(action))
        self.movement_action(action)
        time.sleep(0.5)
        # Update position
        self.pos = self.pepper.getPosition()
        start_moving = time.time()
        # Low level control to verify movement
        while ((abs(prevX-self.pos[0]) < 0.3) and
                (abs(prevY-self.pos[1]) < 0.3) and
                (abs(prevW-self.pos[2]) < m.pi/4)) and ((time.time() - start_moving) < 1.5):
            # Retry pepper movement
            print('Spaming Pepper movement')
            self.movement_action(action)
            self.pos = self.pepper.getPosition()
            time.sleep(0.001)

        # Stop Pepper movement
        self.movement_action(0, 0)
        prevX, prevY, prevW = self.pepper.getPosition()
        time.sleep(0.1)
        # Update position
        self.pos = self.pepper.getPosition()
        start_stoping = time.time()
        # Low level control to verify stop
        while ((abs(prevX-self.pos[0]) > 0.001) or
               (abs(prevY-self.pos[1]) > 0.001) or
               (abs(prevW-self.pos[2]) > 0.001)) and ((time.time() - start_stoping) < 0.5):

            print('Spaming Pepper STOP')
            self.movement_action(0, 0)
            prevX, prevY, prevW = self.pepper.getPosition()
            time.sleep(0.1)
            self.pos = self.pepper.getPosition()
        # Print position
        print('Pepper stopped moving: ' +str(round(self.pos[0],1)) + ', ' + str(round(self.pos[1],1)) + ', ' + str(round(self.pos[2],1)))

    #  ------------------------ STATE  ------------------------
    def update_state(self):
        state = []
        # Get image and mask for the red sphere
        mask = self.get_mask()
        # Get lasers
        lasers = self.get_laser()
        # Determine of the number of pixels in the mask and normalize
        size = (np.count_nonzero(mask))/1700
        # Minimun threshold
        if size > 0.05:
            # Determine position of the mask and normalize between -1 and 1
            posX = np.average(np.nonzero(mask)[1])/160 - 1
            # Return state
            state.append(np.array([size, posX]))
            state.append(lasers)
            print('(State) Area: ' + str(round(size, 3)) +
                  ', PosX: ' + str(round(posX, 3)))
            return state
        else:
            # Return if lost
            state.append(np.array([0, 0]))
            state.append(lasers)
            print('(State) LOST')
            return state

    # Get mask
    def get_mask(self):
        # Subscribing to the bottom RGB camera
        handle = self.pepper.subscribeCamera(PepperVirtual.ID_CAMERA_TOP)
        # Retrieving and displaying the synthetic image using OpenCV
        img = self.pepper.getCameraFrame(handle)
        # cv2.imshow("Synthetic top camera", img)
        # cv2.waitKey(500)
        # Adding a mask to image to get red pixels
        lower = np.array([0, 0, 100])
        upper = np.array([150, 150, 255])
        mask = cv2.inRange(img, lower, upper)
        mask = mask/255
        # plt.imshow(mask, cmap='gray')
        # plt.show(block=False)
        # plt.pause(0.2)
        self.pepper.unsubscribeCamera(handle)
        return mask

    # Get Depth Camera frame
    def get_depth(self):
        # Subscribing to the bottom RGB camera
        handle = self.pepper.subscribeCamera(PepperVirtual.ID_CAMERA_DEPTH)
        # Retrieving and displaying the synthetic image using OpenCV
        img = self.pepper.getCameraFrame(handle)
        # Resize image
        img = cv2.resize(img, (80, 60), interpolation=cv2.INTER_AREA)
        img = cv2.blur(img, (3, 3))
        img = img/8000
        # plt.imshow(img, cmap='gray')
        # plt.show(block=False)
        # plt.pause(0.2)
        # plt.close()
        self.pepper.unsubscribeCamera(handle)

        return img

    # Get laser information
    def get_laser(self):
        mergedLaser = []
        mergedLaser = np.array(self.pepper.getRightLaserValue(), dtype=np.float32)
        mergedLaser = np.append(mergedLaser, np.array(self.pepper.getFrontLaserValue(), dtype=np.float32))
        mergedLaser = np.append(mergedLaser, np.array(self.pepper.getLeftLaserValue(), dtype=np.float32))

        return mergedLaser/3

    def get_local_map(self):
        # Clear local map
        local_map = np.zeros((60, 80))
        # Fill local map with observations (right  laser)
        laser = np.array(self.pepper.getRightLaserValue(), dtype=np.float32)/3
        theta = m.pi/6
        for i, r in enumerate(laser):
            if r < 1:
                x = int(40*r*m.cos(theta - i*m.pi/45)) + 40
                y = int(40*r*m.sin(theta - i*m.pi/45)) + 20
                local_map[y, x] = 1

        # Fill local map with observations (front  laser)
        laser = np.array(self.pepper.getFrontLaserValue(), dtype=np.float32)/3
        theta = 2*m.pi/3
        for i, r in enumerate(laser):
            if r < 1:
                x = int(40*r*m.cos(theta - i*m.pi/45)) + 40
                y = int(40*r*m.sin(theta - i*m.pi/45)) + 20
                local_map[y,  x] = 1

        # Fill local map with observations (left  laser)
        laser = np.array(self.pepper.getLeftLaserValue(), dtype=np.float32)/3
        theta = 7*m.pi/6
        for i, r in enumerate(laser):
            if r < 1:
                x = int(40*r*m.cos(theta - i*m.pi/45)) + 40
                y = int(40*r*m.sin(theta - i*m.pi/45)) + 20
                local_map[y, x] = 1

        # plt.imshow(local_map, cmap='gray')
        # plt.show(block=False)
        # plt.pause(0.5)
        return local_map

    #  ------------------------ REWARD  ------------------------
    def get_reward(self, prevState, newState, step):
        # Variables
        reward = -1

        goalReached = False
        outOfBounds = False
        lostForTooLong = False
        collision = False
        maxItReached = False

        # Change in horizontal position
        deltaPosX = abs(self.prev_state[0][1]) - abs(self.new_state[0][1])

        # Tracking lost state
        if all(newState[0] == prevState[0]):
            # reward = 0
            # Counter if lost
            if self.lost:
                self.lostCount += 1
        # Reward for getting lost
        elif prevState[0][0] != 0 and newState[0][0] == 0:
            # reward = -2
            self.lost = True
        # Reward for founding the objective
        elif prevState[0][0] == 0 and newState[0][0] != 0:
            # reward = 1
            self.lost = False
            # Resets lost counter
            self.lostCount = 0

        # Reward for getting closer to the objective
        if (newState[0][0] > (prevState[0][0]+0.01)):
            reward += 10*newState[0][0]
        elif (newState[0][0] < (prevState[0][0]-0.01)):
            reward -= 10*prevState[0][0]

        # Reward for getting near an obstacle
        laser = newState[1]
        if any(laser < 0.5/3):
            reward -= 60*(0.5/3 - np.min(laser))

        # Update position
        self.pos = self.pepper.getPosition()
        # Verify if robot reach goal
        if (newState[0][0] > 0.99) and (abs(newState[0][1]) < 0.5):
            goalReached = True
            print('EPISODE TERMINATED: Robot reached goal')
            # self.endCause[0] += 1
            reward = 100
        # Verify if robot out of bounds
        if ((self.pos[0] > MAX_X) or (self.pos[1] > MAX_Y) or
                (self.pos[0] < MIN_X) or (self.pos[1] < MIN_Y)):
            outOfBounds = True
            reward = -75
            print('EPISODE TERMINATED: Robot out of bounds')
            # self.endCause[1] += 1
        # Verify if lost for too long
        if self.lostCount > self.max_lost:
            lostForTooLong = True
            reward = -75
            print(' EPISODE TERMINATED: Robot lost for too long')
            # self.endCause[2] += 1

        # Verify if collision
        dToObstacle = 10
        if self.pos[1] < -1.7 or self.pos[1] > 1.7:
            dToObstacle = abs(self.pos[0])
        # dToObstacle = m.sqrt((self.pos[0] - self.obstacleX)**2 +
        #                     (self.pos[1] - self.obstacleY)**2)

        dToHuman = []
        for i in range(0, 2):
            dToHuman.append(m.sqrt((self.pos[0] - self.humanX[i])**2 +
                                   (self.pos[1] - self.humanY[i])**2))

        minToHuman = np.amin(dToHuman)
        print('Distancia a la persona más cercana: '+str(round(minToHuman, 2)))

        if dToObstacle < 0.35 or minToHuman < 0.35:
            collision = True
            reward = -75
            print('EPISODE TERMINATED: Robot collide with an obstacle')
            # self.endCause[3] += 1

        # Verify if max iterations reached
        if step > self.max_it:
            maxItReached = True
            reward = -25
            print('EPISODE TERMINATED: Max iterations reached')
            # self.endCause[4] += 1

        # Print final reward
        print('Lost Counter: ' + str(self.lostCount))
        print('(Reward) r: ' + str(int(reward)/100))
        # End episode condition
        endEpisode = maxItReached or outOfBounds or goalReached or lostForTooLong or collision

        return (reward/100), endEpisode


def main():
    # Create log dir
    log_dir = "/tmp/DQN1/"
    os.makedirs(log_dir, exist_ok=True)
    # Pepper Training Env
    env = PepperSimEnv()
    env = Monitor(env, log_dir)
    # Create the callback: check every 100 steps
    callback = SaveOnBestTrainingRewardCallback(check_freq=99, log_dir=log_dir)
    # Model
    cnn_model = DQN(CnnPolicy, env, buffer_size=1000, exploration_fraction=0.5,
                    exploration_final_eps=0.025, exploration_initial_eps=1.0,
                    train_freq=1, batch_size=32, double_q=True,
                    learning_starts=1001, target_network_update_freq=250,
                    prioritized_replay=True, verbose=1)
    # Training
    cnn_model.learn(total_timesteps=50000, callback=callback)


if __name__ == "__main__":
    # Main function
    main()
